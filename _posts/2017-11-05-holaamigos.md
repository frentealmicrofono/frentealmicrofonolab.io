---
layout: post  
title: "Saludo a los oyentes"  
date: 2017-11-05  
categories: blog  
image: images/logo.png  
podcast_link: 
tags: [blog, Frente al Micrófono]  
comments: true 
---
Aquí puedes añadir las notas del programa  
![#Logo](https://frentealmicrofono.gitlab.io/images/logo.png)

Recuerda que puedes **contactar** con nosotros de las siguientes formas:

+ Twitter: <https://twitter.com/frenteal_micro>
+ Correo: <frentealmicrofono@disroot.com>
+ Web: <https://frentealmicrofono.gitlab.io/>
+ Telegram: <https://t.me/frentealmicrofono>
+ Youtube: <https://www.youtube.com/frentealmicrofono>
+ Feed Podcast: <https://frentealmicrofono.gitlab.io/feed>
