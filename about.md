---
layout: page
title: Acerca de
permalink: /about/
---

Añade toda la info que quieras compartir

Recuerda que puedes **contactar** con nosotros de las siguientes formas:  
(ejemplo)

+ Twitter: <https://twitter.com/frenteal_micro>
+ Correo: <frentealmicrofono@disroot.org>
+ Web: <https://frentealmicrofono.gitlab.io/>
+ Telegram: <https://t.me/frentealmicrofono>
+ Feed Podcast: <https://frentealmicrofono.gitlab.io/feed>
